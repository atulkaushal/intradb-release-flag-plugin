# Sample plugin #

Intradb Release Flag Plugin

# Building #

To build the plugin, run the following command from within the plugin folder:

```bash
./gradlew jar
```

On Windows, you may need to run:

```bash
gradlew jar
```

If you haven't previously run this build, it may take a while for all of the dependencies to download.

You can verify your completed build by looking in the folder **build/libs**. It should contain a file named something like **intradb-release-flag-plugin-1.0.0-SNAPSHOT.jar**. This is the plugin jar that you can install in your XNAT's **plugins** folder.

