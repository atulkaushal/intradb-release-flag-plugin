
if (typeof INTRADB === 'undefined') {
	INTRADB = {};
}
if (typeof INTRADB.releaseflagconfig === 'undefined') {
	INTRADB.releaseflagconfig = { 
		QUALITYLABELS_URL: "/data/projects/" + XNAT.data.context.project + "/config/scan-quality/labels",
		CONFIG_URL: "/data/projects/" + XNAT.data.context.project + "/config/releaseOverride/configuration" 
	};
}


INTRADB.releaseflagconfig.initialize = function() {

	XNAT.spawner.spawn(INTRADB.releaseflagconfig.spawnConfigPanel()).render($("#release_flag_div"));
	$("#release_flag_div").find(".submit.save.btn").on('click', function(e){
		e.preventDefault();
		INTRADB.releaseflagconfig.saveConfig();
		
	});
	$("#release_flag_div").find(".revert.btn").on('click', function(e){
		e.preventDefault();
		INTRADB.releaseflagconfig.reloadConfig();
		
	});
	// populate release flag configuration
	$.ajax({
		type : "GET",
		url:serverRoot + INTRADB.releaseflagconfig.QUALITYLABELS_URL + "?contents=true",
		cache: false,
		async: true,
		context: this,
		dataType: 'text'
	})
	.done(function(data, textStatus,jqXHR) {

		$('#scanQualtyLabelsText').val(data);
		INTRADB.releaseflagconfig.modalityConfig();

	})
	.fail(function(data, textStatus,jqXHR) {
		if (data.status != 404) {
			$("#release_flag_div").html("<h3>ERROR:  Could not retreive configuration</h3>");
		} else {
			INTRADB.releaseflagconfig.modalityConfig();
		}
	})

}

INTRADB.releaseflagconfig.modalityConfig = function() {

		$.ajax({
			type : "GET",
			url:serverRoot + INTRADB.releaseflagconfig.CONFIG_URL + "?contents=true",
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		})
		.done(function(data, textStatus,jqXHR) {
			var dataArr = [];
			if ($.isArray(data)) {
				dataArr = data;
			} else {
				try {
					var tempV = JSON.parse(dataArr);
					if ($.isArray(tempV)) {
						dataArr = tempV;
					}
				} catch (e) {
					// Do nothing
				}
			}
			$(dataArr).each(function(iter) {
				if (this["modality"]) {
					var modalityVar = this;
					var spawn = XNAT.spawner.spawn(INTRADB.releaseflagconfig.addModalityPanel()).render($("#rfModalityDiv"));
					$("input[name='releaseFlagModality']").each(function() {
						var panelBody = $(this).closest(".panel-body");
						if ($(panelBody).find("input[name='releaseFlagModality']").val()) {
							return;
						}
						$(panelBody).find("input[name='releaseFlagModality']").val(modalityVar["modality"]);
						$(panelBody).find("input[name='releaseFlagScanTypeRegex']").val(modalityVar["scanTypeRegex"]);
						$(panelBody).find("input[name='releaseFlagSeriesDescRegex']").val(modalityVar["seriesDescRegex"]);
						$(panelBody).find("input[name='releaseFlagQualityRatings']").val(modalityVar["qualityRatings"]);
						return false;
					});
				}
			});
		})
		.fail(function(data, textStatus,jqXHR) {
			if (data.status != 404) {
				$("#release_flag_div").html("<h3>ERROR:  Could not retreive configuration</h3>");
			} 
		})

}

INTRADB.releaseflagconfig.reloadConfig = function() {
	$("#release_flag_div").html('');
	INTRADB.releaseflagconfig.initialize();
}

INTRADB.releaseflagconfig.saveConfig = function() {

	$.ajax({
		type : "PUT",
		url:serverRoot + INTRADB.releaseflagconfig.QUALITYLABELS_URL + "?XNAT_CSRF=" + window.csrfToken,
		cache: false,
		async: true,
		context: this,
		data:  $('#scanQualtyLabelsText').val(),
		contentType:  "text/plain",
		dataType: 'text'
	})
	.done(function(data, textStatus,jqXHR) {
		var configArr = [];
		$("input[name='releaseFlagModality']").each(function() {
			var panelBody = $(this).closest(".panel-body");
			var configModality = {};
			if (!$(panelBody).find("input[name='releaseFlagModality']").val()) {
				return;
			}
			configModality['modality']=$(panelBody).find("input[name='releaseFlagModality']").val();
			configModality['scanTypeRegex']=$(panelBody).find("input[name='releaseFlagScanTypeRegex']").val();
			configModality['seriesDescRegex']=$(panelBody).find("input[name='releaseFlagSeriesDescRegex']").val();
			configModality['qualityRatings']=$(panelBody).find("input[name='releaseFlagQualityRatings']").val();
			configArr.push(configModality);
		});
		$.ajax({
			type : "PUT",
			url:serverRoot + INTRADB.releaseflagconfig.CONFIG_URL + "?inbody=true&XNAT_CSRF=" + window.csrfToken,
			cache: false,
			async: true,
			context: this,
			data:  JSON.stringify(configArr),
			contentType: "application/text; charset=utf-8"
		})
		.done(function(data, textStatus,jqXHR) {
			 XNAT.ui.banner.top(2000, 'Data saved successfully.', 'success');
		})
		.fail(function(data, textStatus,jqXHR) {
			// Do nothing for now
		})

	})
	.fail(function(data, textStatus,jqXHR) {
		// Do nothing for now
	})

}

INTRADB.releaseflagconfig.spawnConfigPanel = function() {

	function configPanel(contents) {
		return {
			id: 'intradbReleaseFlag',
			kind: 'panel.form',
			label: 'IntraDB Release Flag Plugin Configuration',
			header: false,
			contents: {
				"sqlSubhead": scanQualityLabelsSubhead(),
				"sqlText": scanQualityLabelsText(),
				"rfSubhead": releaseFlagSubhead(),
				"btn": addModalityButton(),
				"btn": addModalityButton(),
			}
		}
	}
	function scanQualityLabelsSubhead() {
		return {
			kind: 'panel.subhead',
			label: 'Project Scan Quality Labels Configuration'
		}
	}
	function scanQualityLabelsText() {
		return {
			kind: 'panel.input.text',
			label: 'Scan Quality Labels',
			id: 'scanQualtyLabelsText',
			size: 45,
			placeholder: "undetermined,excellent,good,fair,poor,usable,unusable",
			description: "Comma-separated list of scan quality labels to be used on this project.",
		}
	}
	function releaseFlagSubhead() {
		return {
			kind: 'panel.subhead',
			label: 'Project Release Flag Usage Configuration'
		}
	}
	function addModalityButton() {
		return {
			id: 'intradbReleaseFlagAddModalityButton',
			kind: 'button',
			html: 'Add Release Flag Modality',
			after: '<div id="rfModalityDiv" style="width:100%;padding-top:10px;"></div>',
			onclick: function() {
				var doContinue = true;
				$("input[name='releaseFlagModality'],input[name='releaseFlagQualityRatings]").each(function() {
					if (!$(this).val()) {
						doContinue = false;
						return false;
					}
				});
				if (doContinue) {
					XNAT.spawner.spawn(INTRADB.releaseflagconfig.addModalityPanel()).render($("#rfModalityDiv"));
				}
				
			}
		}
	}
	return {
		root: configPanel()
	};	

}

INTRADB.releaseflagconfig.addModalityPanel = function() {

	function modalityPanel(contents) {
		return {
			kind: 'panel',
			name: 'releaseFlagModalityPanel',
			header: false,
			contents: {
				"xsiText": xsiTypeText(),
				"stRegex": scanTypeRegex(),
				"sdRegex": seriesDescRegex(),
				"qRatings": qualityRatings()
			}
		}
	}
	function xsiTypeText() {
		return {
			kind: 'panel.input.text',
			name: 'releaseFlagModality',
			label: 'Modality',
			size: 40,
			placeholder: "xnat:mrSessionData"
		}
	}
	function scanTypeRegex() {
		return {
			kind: 'panel.input.text',
			name: 'releaseFlagScanTypeRegex',
			label: 'Scan Type Regex',
			placeholder: "[Tt][12][Ww]",
			size: 40,
			description: "Optional Java regular expression used for matching scan types targeted for release flag use"
		}
	}
	function seriesDescRegex() {
		return {
			kind: 'panel.input.text',
			name: 'releaseFlagSeriesDescRegex',
			label: 'Series Desc Regex',
			placeholder: "[Tt][12][Ww]",
			size: 40,
			description: "Optional Java regular expression used for matching series descriptions targeted for release flag use"
		}
	}
	function qualityRatings() {
		return {
			kind: 'panel.input.text',
			name: 'releaseFlagQualityRatings',
			label: 'Quality Ratings',
			placeholder: "excellent,good,fair,poor",
			size: 40,
			description: "Comma-separated list of uality ratings valid for setting release flags (normally a subset of the project release flags)"
		}
	}
	return {
		root: modalityPanel()
	};	

}


